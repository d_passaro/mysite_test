package com.mysite.core.models.impl;

import com.mysite.core.models.Byline;
import com.mysite.core.models.OSGiConfigDemo;
import com.mysite.core.service.OSGiConfigModule;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.osgi.service.component.annotations.Reference;

@Model(
        adaptables = {SlingHttpServletRequest.class},
        adapters = {OSGiConfigDemo.class},
        resourceType = {OSGiConfigDemoImpl.RESOURCE_TYPE},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
)
public class OSGiConfigDemoImpl implements OSGiConfigDemo {

    protected static final String RESOURCE_TYPE = "mysite/components/content/osgiconfigdemo";


    @OSGiService
    OSGiConfigModule config;

    @Override
    public int getServiceId() {
        return config.getServiceId();
    }

    @Override
    public String getServiceNameModule() {
        return config.getServiceName();
    }

    @Override
    public String getServiceURL() {
        return config.getServiceURL();
    }
}
