package com.mysite.core.utils;

import com.day.cq.commons.Externalizer;
import org.apache.sling.api.resource.ResourceResolver;

public class CartesiamUtils {

    private static final String CONTENT_ROOT = "/content";

    public static String externalizeLink(String link, Externalizer externalizer, ResourceResolver resolver){
        if(isLinkInternal(link)){
            return externalizer.externalLink(resolver, Externalizer.LOCAL, link) + ".html";
        }else{
            return link;
        }
    }

    public static boolean isLinkInternal(String link){
        return link.startsWith(CONTENT_ROOT);
    }
}
