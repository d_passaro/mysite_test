package com.mysite.core.service.impl;

import com.adobe.cq.commerce.pim.common.Csv;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;
import com.google.common.io.CharStreams;
import com.mysite.core.service.ImportDataToUnomi;
import com.mysite.core.service.OSGiConfigModule;
import com.mysite.core.utils.CsvUtils;
import com.mysite.core.utils.FileUtil;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;


@Component(
        immediate = true,
        service = {ImportDataToUnomi.class}
)

public class ImportDataToUnomiImpl implements ImportDataToUnomi {

    private static final String CSV_SEPARATOR = ",";

    private static final Logger log = LoggerFactory.getLogger(ImportDataToUnomi.class);



    @Override
    public Map<String, String> importDataToUnomiFromDam(String path, ResourceResolver resourceResolver) throws IOException {
        Resource res = resourceResolver.getResource(path);
        if(res == null){
            log.debug("Error on resource adaption");
            return null;
        }
        Asset asset = res.adaptTo(Asset.class);

        if(asset == null){
            log.debug("Error on asset adaption");
            return null;
        }
        Rendition rendition = asset.getOriginal();
        InputStream inputStream = rendition.adaptTo(InputStream.class);

        File file = FileUtil.createTempFileFromInputStream("nome","nomeFolder", inputStream);

        List<String[]> csvRows = CsvUtils.getCsvRowsFromFile(file, CSV_SEPARATOR, true);

        CsvUtils.cleanUpTMPFile(file);


        return null;
    }


}
