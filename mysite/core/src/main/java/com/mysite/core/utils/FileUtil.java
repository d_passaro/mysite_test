package com.mysite.core.utils;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public final class FileUtil {
    private static final Logger LOG = LoggerFactory.getLogger(FileUtil.class);

    private static final double SPACE_KB = 1024;

    private static final double SPACE_MB = 1024 * SPACE_KB;

    private static final double SPACE_GB = 1024 * SPACE_MB;

    private static final double SPACE_TB = 1024 * SPACE_GB;

    private FileUtil() {}

    public static String bytesToString(final long sizeInBytes) {
        final NumberFormat nf = new DecimalFormat();
        nf.setMaximumFractionDigits(2);
        if (sizeInBytes < SPACE_KB) {
            return nf.format(sizeInBytes) + " Byte(s)";
        } else if (sizeInBytes < SPACE_MB) {
            return nf.format(sizeInBytes / SPACE_KB) + " KB";
        } else if (sizeInBytes < SPACE_GB) {
            return nf.format(sizeInBytes / SPACE_MB) + " MB";
        } else if (sizeInBytes < SPACE_TB) {
            return nf.format(sizeInBytes / SPACE_GB) + " GB";
        } else {
            return nf.format(sizeInBytes / SPACE_TB) + " TB";
        }
    }

    public static byte[] zipBytes(String filename, byte[] input) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(baos);
        try {
            ZipEntry entry = new ZipEntry(filename);
            entry.setSize(input.length);
            zos.putNextEntry(entry);
            zos.write(input);
            zos.closeEntry();
            zos.flush();
            baos.flush();
            zos.close();
            baos.close();
        } catch (IOException e) {
            LOG.error("Can't put resource {} into zip file", filename, e);
        }
        return baos.toByteArray();
    }

    /**
     * Create temporary file folder for temp files. It will be deleted after the application shutdown
     *
     */
    public static File createTempFileFolder(String tmpParentDir) {
        String tmpFolderPath = System.getProperty("java.io.tmpdir") + File.separator + tmpParentDir;
        File tmpFolderFile = new File(tmpFolderPath);
        if (!tmpFolderFile.exists()) {
            LOG.debug("Creating tmp folder : {}", tmpFolderPath);
            tmpFolderFile.mkdirs();
        }
        tmpFolderFile.deleteOnExit();
        return tmpFolderFile;
    }


    /**
     * Delete file from the system
     * @param uploadFile file to delete
     */
    public static void deleteFile(File uploadFile) {
        if (uploadFile != null) {
            try {
                Files.deleteIfExists(uploadFile.toPath());
            } catch (IOException e) {
                LOG.error("Error during deleting temp report file", e.getMessage(), e);
            }
        }
    }

    /**
     * Create temp file from Input Stream at the specified root folder
     * @param fileName Name of the created file
     * @param rootFolderName Root folder to store file
     * @param inputStream Data represented by input stream
     * @return Created file
     */
    public static File createTempFileFromInputStream(String fileName, String rootFolderName, InputStream inputStream) {
        File tempFileFolder = FileUtil.createTempFileFolder(rootFolderName);
        File uploadFile = new File(tempFileFolder.getPath() + "/" + fileName);
        try {
            LOG.info("Writing  translation report data to file:{}", uploadFile.getPath());
            FileUtils.copyInputStreamToFile(inputStream, uploadFile);
        } catch (IOException e) {
            LOG.error("Error during writing translation report data to file", e.getMessage(), e);
            return null;
        }
        return uploadFile;
    }
}
