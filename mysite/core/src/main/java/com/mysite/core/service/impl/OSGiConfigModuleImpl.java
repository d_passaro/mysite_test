package com.mysite.core.service.impl;

import com.mysite.core.config.OsgiConfig;
import com.mysite.core.service.OSGiConfigModule;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;

@Component(
        immediate = true,
        service = {OSGiConfigModule.class}
)
@Designate(
        ocd = OsgiConfig.class
)
public class OSGiConfigModuleImpl implements OSGiConfigModule {

    private OsgiConfig config;

    @Activate
    protected void activate(OsgiConfig config) {
        this.config = config;
    }


    @Override
    public int getServiceId() {
        return this.config.serviceID();
    }

    @Override
    public String getServiceName() {
        return this.config.serviceName();
    }

    @Override
    public String getServiceURL() {
        return this.config.serviceURL();
    }
}
