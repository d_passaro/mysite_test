package com.mysite.core.models.impl;

import com.adobe.cq.wcm.core.components.models.Image;
import com.day.cq.commons.Externalizer;
import com.mysite.core.models.Header;
import com.mysite.core.models.Video;
import com.mysite.core.utils.CartesiamUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.factory.ModelFactory;

import javax.annotation.PostConstruct;


@Model(
        adaptables = {SlingHttpServletRequest.class},
        adapters = {Header.class},
        resourceType = {HeaderImpl.RESOURCE_TYPE},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
)
public class HeaderImpl implements Header {

    protected static final String RESOURCE_TYPE = "mysite/components/content/header";

    @Self
    private SlingHttpServletRequest request;

    @ValueMapValue
    private String nanoEdgeImage;

    @ValueMapValue
    private String homepageLink;

    @ValueMapValue
    private String useCaseLink;

    @ValueMapValue
    private String useCaseTitle;

    private ResourceResolver resolver;
    private Externalizer externalizer;

    @PostConstruct
    private void init(){
        resolver = request.getResourceResolver();
        externalizer = resolver.adaptTo(Externalizer.class);
    }

    @Override
    public String getNanoEdgeImage() {
        return nanoEdgeImage;
    }

    @Override
    public String getHomepageLink() {
       return CartesiamUtils.externalizeLink(homepageLink, externalizer, resolver);
    }

    @Override
    public String getUseCaseLink() {
        return CartesiamUtils.externalizeLink(useCaseLink, externalizer, resolver);
    }

    @Override
    public String getUseCaseTitle() { return useCaseTitle; }

}
