$(document).ready(function () {
    initializeScrollEvent();
});


function initializeScrollEvent() {
    window.onscroll = function (e) {
        //called when the window is scrolled.
        if ($(window).scrollTop() < 1) {
            showHeader();
        } else {
            hideHeader();
        }
    }
}

function hideHeader() {
    var header = $("#header-cartesiam");
    var body = $("#body-cartesiam");

    if (header) {
        //Change style and class of the header in order to hide it
        header.addClass('v-app-bar--hide-shadow v-app-bar--is-scrolled');
        header.css("transform", "translateY(-48px)");
    }

    if (body) {
        body.css("padding", "0px 0px 216px");
    }

}

function showHeader() {
    var header = $("#header-cartesiam");
    var body = $("#body-cartesiam");

    if (header) {
        header.removeClass('v-app-bar--hide-shadow v-app-bar--is-scrolled');
        header.css("transform", "translateY(0)");
    }

    if (body) {
        body.css("padding", "48px 0px 216px");
    }
}