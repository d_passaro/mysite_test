package com.mysite.core.models.impl;

import com.day.cq.commons.Externalizer;
import com.mysite.core.models.DownloadNanoEdge;
import com.mysite.core.utils.CartesiamUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;

@Model(
        adaptables = {SlingHttpServletRequest.class},
        adapters = {DownloadNanoEdge.class},
        resourceType = {DownloadNanoEdgeImpl.RESOURCE_TYPE},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
)
public class DownloadNanoEdgeImpl implements DownloadNanoEdge{

    protected static final String RESOURCE_TYPE = "mysite/components/content/downloadNanoEdge";

    @Self
    private SlingHttpServletRequest request;

    @ValueMapValue
    private String downloadText;

    @ValueMapValue
    private String downloadLink;

    @ValueMapValue
    private String downloadTextButton;

    private ResourceResolver resolver;
    private Externalizer externalizer;

    @PostConstruct
    private void init(){
        resolver = request.getResourceResolver();
        externalizer = resolver.adaptTo(Externalizer.class);
    }

    @Override
    public String getDownloadText() {
        return downloadText;
    }

    @Override
    public String getDownloadTextButton() {
        return downloadTextButton;
    }

    @Override
    public String getDownloadLink() {
        return CartesiamUtils.externalizeLink(downloadLink, externalizer, resolver);
    }
}
