package com.mysite.core.models;

public interface LinkDetails {

    public String getLinkLabel();
    public String getLinkLabelUrl();
}
