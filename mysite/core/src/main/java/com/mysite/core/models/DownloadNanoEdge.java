package com.mysite.core.models;

public interface DownloadNanoEdge {
    public String getDownloadText();
    public String getDownloadTextButton();
    public String getDownloadLink();
}
