package com.mysite.core.models;

public interface Header {
    public String getNanoEdgeImage();
    public String getHomepageLink();
    public String getUseCaseLink();
    public String getUseCaseTitle();
}
