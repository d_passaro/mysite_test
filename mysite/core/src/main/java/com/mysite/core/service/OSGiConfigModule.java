package com.mysite.core.service;

public interface OSGiConfigModule {
    public int getServiceId() ;
    public String getServiceName();
    public String getServiceURL() ;
}
