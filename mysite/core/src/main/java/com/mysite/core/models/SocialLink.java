package com.mysite.core.models;

public interface SocialLink {
    public String getSocialName();
    public String getSocialUrl();
}
