package com.mysite.core.models;

public interface OSGiConfigDemo {

    public int getServiceId();
    public String getServiceNameModule() ;
    public String getServiceURL() ;
}
