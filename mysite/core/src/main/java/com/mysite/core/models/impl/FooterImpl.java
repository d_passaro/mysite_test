package com.mysite.core.models.impl;

import com.day.cq.commons.Externalizer;
import com.mysite.core.models.Footer;
import com.mysite.core.models.LinkDetails;
import com.mysite.core.models.SocialLink;
import com.mysite.core.utils.CartesiamUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import java.util.List;

@Model(
        adaptables = {SlingHttpServletRequest.class},
        adapters = {Footer.class},
        resourceType = {HeaderImpl.RESOURCE_TYPE},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
)
public class FooterImpl implements Footer {

    protected static final String RESOURCE_TYPE = "mysite/components/content/footer";

    @Self
    private SlingHttpServletRequest request;

    @ValueMapValue
    private String downloadText;

    @ValueMapValue
    private String downloadLink;

    @ValueMapValue
    private String downloadTextButton;

    @ValueMapValue
    private String copyrightText;

    @ValueMapValue
    private String privacyLink;

    @ValueMapValue
    private String privacyText;

    @ChildResource
    private List<LinkDetails> footerLinks;

    @ChildResource
    private List<SocialLink> socialLinks;

    private ResourceResolver resolver;
    private Externalizer externalizer;

    @PostConstruct
    private void init(){
        resolver = request.getResourceResolver();
        externalizer = resolver.adaptTo(Externalizer.class);
    }

    @Override
    public String getDownloadText() {
        return downloadText;
    }

    @Override
    public String getDownloadTextButton() { return downloadTextButton; }

    @Override
    public String getDownloadLink() {
        return CartesiamUtils.externalizeLink(downloadLink, externalizer, resolver);
    }

    @Override
    public String getCopyrightText() { return copyrightText; }

    @Override
    public String getPrivacyLink() {
        return CartesiamUtils.externalizeLink(privacyLink, externalizer, resolver);
    }

    @Override
    public String getPrivacyText() { return privacyText; }

    @Override
    public List<LinkDetails> getFooterLinks() { return footerLinks; }

    @Override
    public List<SocialLink> getSocialLinks() { return socialLinks; }
}
