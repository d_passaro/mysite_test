package com.mysite.core.models;

public interface UseCaseHeading {
    public String getCategory();
    public String getTitle();
    public String getSubtitle();
    public boolean isConfigured();
}
