package com.mysite.core.servlets;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.mysite.core.service.OSGiConfigModule;
import org.apache.http.HttpStatus;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.servlets.annotations.SlingServletPaths;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.*;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

@Component(service = Servlet.class, property = {Constants.SERVICE_DESCRIPTION + "=Simple Demo Servlet",
        "sling.servlet.methods=" + HttpConstants.METHOD_POST, "sling.servlet.paths=" + "/bin/example/createNode"})
public class PathServlet extends SlingAllMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(PathServlet.class);

    @Reference
    OSGiConfigModule osGiConfigModule;

    @Override
    protected void doPost(final SlingHttpServletRequest req, final SlingHttpServletResponse resp) throws ServletException, IOException {
        final ResourceResolver resourceResolver = req.getResourceResolver();

        try {

            String resourcePath = req.getParameter("path");
            String property = req.getParameter("message");
            Resource res = resourceResolver.getResource(resourcePath);

            Session session = resourceResolver.adaptTo(Session.class);

            if (session == null) {
                resp.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            }

            //Create a node that represents the root node
            Node root = session.getRootNode();

            // Store content
            //TODO /etc/node1/node2/... addNode(etc) addNode(node1)
            //TODO /etc/node1/node2 -- > node1 node2
            //TODO [node1, node2...] for/foreach

            // Getting servlet request URL
            String url = req.getRequestURL().toString();

            // Getting servlet request path
            String path = req.getRequestPathInfo().getResourcePath();

            String[] splittedPath = resourcePath.split("/");

            Node parentNode = root;

            for (int i = 0; i < splittedPath.length; i++) {
                if (!parentNode.hasNode(splittedPath[i]))
                    parentNode = parentNode.addNode(splittedPath[i]);
                else
                    parentNode = parentNode.getNode(splittedPath[i]);
            }

            //for (String a : splittedPath)
            //   root.addNode(a);

            parentNode.setProperty("message",property);
            parentNode.hasProperty("message");

            // Retrieve content
            Node node = root.getNode("adobe/day");
            System.out.println(node.getPath());
            System.out.println(node.getProperty("message").getString());

            // Save the session changes and log out
            session.save();
            session.logout();

        } catch (RepositoryException e) {
            LOG.info("\n ERROR {} ", e.getMessage());
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }

        resp.setStatus(HttpServletResponse.SC_OK);
    }
}


