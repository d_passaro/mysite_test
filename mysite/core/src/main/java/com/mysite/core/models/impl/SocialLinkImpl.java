package com.mysite.core.models.impl;

import com.day.cq.commons.Externalizer;
import com.mysite.core.models.SocialLink;
import com.mysite.core.utils.CartesiamUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;


@Model(
        adaptables = {Resource.class},
        adapters = {SocialLink.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
)
public class SocialLinkImpl implements SocialLink {

    @Self
    private Resource resource;

    @ValueMapValue
    private String socialName;

    @ValueMapValue
    private String socialUrl;

    private ResourceResolver resolver;
    private Externalizer externalizer;

    @PostConstruct
    private void init(){
        resolver = resource.getResourceResolver();
        externalizer = resolver.adaptTo(Externalizer.class);
    }


    @Override
    public String getSocialName() {
        return socialName;
    }

    @Override
    public String getSocialUrl() {
        return CartesiamUtils.externalizeLink(socialUrl, externalizer, resolver);
    }
}
