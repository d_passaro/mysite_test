package com.mysite.core.models.impl;


import com.mysite.core.models.Video;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.factory.ModelFactory;


@Model(
        adaptables = {SlingHttpServletRequest.class},
        adapters = {Video.class},
        resourceType = {VideoImpl.RESOURCE_TYPE},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
)
public class VideoImpl implements Video {

    protected static final String RESOURCE_TYPE = "mysite/components/content/video";

    @Self
    private SlingHttpServletRequest request;

    @OSGiService
    private ModelFactory modelFactory;

    @ValueMapValue
    private String title;

    @ValueMapValue
    private String video;

    @ValueMapValue
    private String link;

    @ValueMapValue
    private String play;

    @ValueMapValue
    private String pause;

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getPlay() {
        return play;
    }

    @Override
    public String getPause() {
        return pause;
    }

    @Override
    public String getLink() {
        return link;
    }
}
