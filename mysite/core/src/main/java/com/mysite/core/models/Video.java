package com.mysite.core.models;

public interface Video {

    String getTitle();
    String getPlay();
    String getPause();
    String getLink();

}
