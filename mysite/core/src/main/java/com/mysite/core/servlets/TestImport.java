package com.mysite.core.servlets;

import com.mysite.core.service.ImportDataToUnomi;
import com.mysite.core.service.OSGiConfigModule;
import com.mysite.core.service.impl.ImportDataToUnomiImpl;
import org.apache.http.HttpStatus;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component(service = Servlet.class, property = {Constants.SERVICE_DESCRIPTION + "=Simple Test Servlet",
        "sling.servlet.methods=" + HttpConstants.METHOD_GET, "sling.servlet.paths=" + "/bin/test/importUnomi"})
public class TestImport extends SlingSafeMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(TestImport.class);

    @Reference
    ImportDataToUnomi importDataToUnomi;

    String pathdam = "/content/dam/recipients_application_peference.csv";

    @Override
    protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse resp) throws ServletException, IOException {
        final ResourceResolver resourceResolver = req.getResourceResolver();

            importDataToUnomi.importDataToUnomiFromDam(pathdam, resourceResolver);

    }
}


