package com.mysite.core.models.impl;

import com.day.cq.commons.Externalizer;
import com.mysite.core.models.LinkDetails;
import com.mysite.core.utils.CartesiamUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;

@Model(
        adaptables = {Resource.class},
        adapters = {LinkDetails.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
)
public class LinkDetailsImpl implements LinkDetails {

    @Self
    private Resource resource;

    @ValueMapValue
    private String linkLabel;

    @ValueMapValue
    private String linkLabelUrl;

    private ResourceResolver resolver;
    private Externalizer externalizer;

    @PostConstruct
    private void init(){
        resolver = resource.getResourceResolver();
        externalizer = resolver.adaptTo(Externalizer.class);
    }

    public String getLinkLabel() {
        return linkLabel;
    }

    public String getLinkLabelUrl() {
        return CartesiamUtils.externalizeLink(linkLabelUrl, externalizer, resolver);
    }
}
