package com.mysite.core.models.impl;

import com.mysite.core.models.UseCaseHeading;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(
        adaptables = {SlingHttpServletRequest.class},
        adapters = {UseCaseHeading.class},
        resourceType = {UseCaseHeadingImpl.RESOURCE_TYPE},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
)
public class UseCaseHeadingImpl implements UseCaseHeading{

    protected static final String RESOURCE_TYPE = "mysite/components/content/use_case-heading";

    @ValueMapValue
    private String useCaseCategory;

    @ValueMapValue
    private String useCaseTitle;

    @ValueMapValue
    private String useCaseSubtitle;



    @Override
    public String getCategory() {
        return useCaseCategory;
    }

    @Override
    public String getTitle() {
        return useCaseTitle;
    }

    @Override
    public String getSubtitle() {
        return useCaseSubtitle;
    }

    @Override
    public boolean isConfigured(){
        return (!useCaseCategory.isEmpty() || !useCaseTitle.isEmpty() || !useCaseSubtitle.isEmpty());
    }
}
