package com.mysite.core.models;


import java.util.List;

public interface Footer {
    public String getDownloadText();
    public String getDownloadTextButton();
    public String getDownloadLink();
    public String getCopyrightText();
    public String getPrivacyLink();
    public String getPrivacyText();
    public List<LinkDetails> getFooterLinks();
    public List<SocialLink> getSocialLinks();
}
