package com.mysite.core.service;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ImportDataToUnomi {
    public Map<String,String> importDataToUnomiFromDam(String path, ResourceResolver resourceResolver) throws IOException;
}
