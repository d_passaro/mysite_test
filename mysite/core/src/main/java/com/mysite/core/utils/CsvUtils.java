package com.mysite.core.utils;

import com.adobe.cq.commerce.pim.common.Csv;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CsvUtils {

    private static final Logger log = LoggerFactory.getLogger(CsvUtils.class);

    public static File createTemporaryFile(String fileName){
        File tmpFolder = FileUtil.createTempFileFolder("MarketoCSVTmpFolder");
        return new File(tmpFolder.getPath() + "/" + fileName);
    }

    public static File createTemporaryFile(String fileName, String folderName){
        File tmpFolder = FileUtil.createTempFileFolder(folderName);
        return new File(tmpFolder.getPath() + "/" + fileName);
    }

    public static void cleanUpTMPFile(File file){
        boolean isFileDeleted = false;
        if (file.exists())
            isFileDeleted = file.delete();
    }

    public static Csv initCSV (OutputStream csvStream, String charset, String fieldSeparator ){

        // Write BOM for utf-8 recognition.
        /*try {
            csvStream.write(239);
            csvStream.write(187);
            csvStream.write(191);
        } catch (IOException e) {
            log.error("Error while writing BOM CSV,", e);
        }*/
        Csv csv = new Csv();
        try {
            csv.writeInit( csvStream , charset );
        } catch (IOException e) {
            log.error("Error while initializing CSV,", e);
            return null;
        }
        if ( !StringUtils.isEmpty(fieldSeparator) )
            csv.setFieldSeparatorWrite( fieldSeparator );
        return csv;

    }

    public static void writeCSVRow(Csv csv, String[] row){
        try {
            csv.writeRow( row );
        } catch (IOException e) {
            log.error("Error while writing CSV Row,", e);
        }
    }

    public static List<String[]> getCsvRowsFromFile(File csvFile, String fieldSeparator, boolean skipHeaderLine) throws IOException {
        List<String[]> csvLines = new ArrayList<>();
        if (csvFile == null)
            return csvLines;
        boolean skipFirstLine = skipHeaderLine;
        InputStream targetStream = new FileInputStream(csvFile);
        Csv inputCsvFile = new Csv();
        if ( !StringUtils.isEmpty(fieldSeparator) )
            inputCsvFile.setFieldSeparatorWrite( fieldSeparator );
        Iterator<String[]> csvInputLines = inputCsvFile.read(targetStream, StandardCharsets.UTF_8.name());
        while (csvInputLines.hasNext()){
            if (skipFirstLine){
                skipFirstLine = false;
                csvInputLines.next();
            }
            csvLines.add(csvInputLines.next());
        }
        return csvLines;
    }
}
